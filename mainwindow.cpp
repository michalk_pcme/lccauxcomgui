#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QtSerialPort/QSerialPortInfo>
#include <QScrollBar>
#include <QFileDialog>
#include <QCheckBox>
#include <QLineEdit>



QT_USE_NAMESPACE

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent), ui(new Ui::MainWindow){

    //this is needed for passing parameters using slots/signals
    qRegisterMetaType<QVector<serialProtocol::messageStc> >("QVector<serialProtocol::messageStc>");
    qRegisterMetaType<serialProtocol::messageStc> ("serialProtocol::messageStc");
    qRegisterMetaType<serialCommands::systemStatsParams_t> ("serialCommands::systemStatsParams_t");
    qRegisterMetaType<QVector<double> > ("QVector<double>");

    ui->setupUi(this);
    connectSignals();
    fillComboBoxes();

}


MainWindow::~MainWindow(){
    delete ui;
}


void MainWindow::connectSignals(){

    connect(ui->PB_connect,        SIGNAL(clicked()), this, SLOT(PB_connect_ButtonClick()));
    connect(ui->PB_refresh_ports,  SIGNAL(clicked()), this, SLOT(PB_refresh_ports_ButtonClick()));

    connect(ui->PB_clear_notifications, SIGNAL(clicked()), this, SLOT(PB_clear_notifications_ButtonClick()));

    connect(&serialThread, SIGNAL(notification(QString, QColor)), this, SLOT(displayNotification(QString, QColor)));
    connect(&serialThread, SIGNAL(receivedData(QByteArray)),      this, SLOT(processMessages(QByteArray)));
    connect(&serialThread, SIGNAL(portOpened()),                  this, SLOT(portOpened()));
    connect(&serialThread, SIGNAL(portClosed()),                  this, SLOT(portClosed()));

    connect(&serCmd, SIGNAL(notification(QString, QColor)),                         this, SLOT(displayNotification(QString, QColor)));
    connect(&serCmd, SIGNAL(sendMessage(serialProtocol::messageStc)),               this, SLOT(sendMessageToSerialPort(serialProtocol::messageStc)));
    connect(&serCmd, SIGNAL(sendDeviceId(quint32)),                                 this, SLOT(displayDeviceId(quint32)));
    connect(&serCmd, SIGNAL(sendSystemStats(serialCommands::systemStatsParams_t)),  this, SLOT(displaySystemStats(serialCommands::systemStatsParams_t)));

    connect(ui->PB_template, SIGNAL(clicked()), this, SLOT(PB_template_ButtonClick()));

    connect(ui->PB_startSensSelfCheck, SIGNAL(clicked()), this, SLOT(PB_startSensSelfCheck_ButtonClick()));

    connect(ui->PB_cal4ma,          SIGNAL(clicked()),            this, SLOT(PB_cal4ma_ButtonClick()));
    connect(ui->PB_cal20ma,         SIGNAL(clicked()),            this, SLOT(PB_cal20ma_ButtonClick()));
    connect(ui->PB_release4_20Override, SIGNAL(clicked()),        this, SLOT(PB_release4_20Override_ButtonClick()));
    connect(ui->periodSpinBox,      SIGNAL(valueChanged(int)),    this, SLOT(override4_20period(int)));
    connect(ui->maSpinBox,          SIGNAL(valueChanged(double)), this, SLOT(override4_20ma(double)));

    connect(ui->PB_relay1on,        SIGNAL(clicked()),      this, SLOT(PB_relay1on_ButtonClick()));
    connect(ui->PB_relay2on,        SIGNAL(clicked()),      this, SLOT(PB_relay2on_ButtonClick()));
    connect(ui->PB_relay1off,       SIGNAL(clicked()),      this, SLOT(PB_relay1off_ButtonClick()));
    connect(ui->PB_relay2off,       SIGNAL(clicked()),      this, SLOT(PB_relay2off_ButtonClick()));
    connect(ui->PB_releaseRelayOverride, SIGNAL(clicked()), this, SLOT(PB_releaseRelayOverride_ButtonClick()));

    ;
    connect(ui->PB_getSystemStats,   SIGNAL(clicked()), this, SLOT(PB_getSystemStats_ButtonClick()));
    connect(ui->PB_resetSystemStats, SIGNAL(clicked()), this, SLOT(PB_resetSystemStats_ButtonClick()));

    connect(ui->PB_getDeviceId,     SIGNAL(clicked()), this, SLOT(PB_getDeviceId_ButtonClick()));

}


void MainWindow::displayNotification(const QString &message, const QColor color){
    addNotification(message,color);
}


void MainWindow::sendMessageToSerialPort(const serialProtocol::messageStc &message){

    serialThread.sendData(this->serPtcl.prepareTXmessage(message));
}


void MainWindow::displayDeviceId(const quint32 &deviceId)
{
    QString str;

    str.sprintf("0x%08X", deviceId);

    ui->TB_deviceId->clear();
    ui->TB_deviceId->setText(str);
}


void MainWindow::displaySystemStats(const serialCommands::systemStatsParams_t &stats)
{
    QString str;

    str.sprintf("%d", stats.proc);
    ui->TB_modProc->setText(str);

    str.sprintf("%d", stats.rxFail);
    ui->TB_modFail->setText(str);

    str.sprintf("%d", stats.sent);
    ui->TB_modSent->setText(str);

    if(0 == stats.eepromHealth) str = "FAIL";
    if(1 == stats.eepromHealth) str = "PASS";
    ui->TB_eepromHealth->setText(str);

    if(0 == stats.flashHealth) str = "FAIL";
    if(1 == stats.flashHealth) str = "PASS";
    ui->TB_flashHealth->setText(str);
}


void MainWindow::addNotification(const QString &message, const QColor &color){

    ui->TB_notification->setTextColor(color);
    ui->TB_notification->insertPlainText(message);
    QScrollBar *bar = ui->TB_notification->verticalScrollBar();
    bar->setValue(bar->maximum());
}


void MainWindow::PB_clear_notifications_ButtonClick(){
    ui->TB_notification->clear();
}


void MainWindow::processMessages(const QByteArray &data){

    QVector<serialProtocol::messageStc> messages = serPtcl.addNewlyReceivedData(data);

    if(messages.isEmpty() == false){
        //process messages
        serCmd.processQueue(messages);
    }
}


void MainWindow::fillComboBoxes(){
    if(!ui) return;

    foreach (const QSerialPortInfo &info, QSerialPortInfo::availablePorts())
        ui->CB_port->addItem(info.portName());

    ui->CB_baud->addItem(QStringLiteral("9600"), QSerialPort::Baud9600);
    ui->CB_baud->addItem(QStringLiteral("19200"), QSerialPort::Baud19200);
    ui->CB_baud->addItem(QStringLiteral("38400"), QSerialPort::Baud38400);
    ui->CB_baud->addItem(QStringLiteral("115200"), QSerialPort::Baud115200);
    ui->CB_baud->setCurrentIndex(3);
    //ui->CB_baud->addItem(tr("Custom"));

    ui->CB_data_bits->addItem(QStringLiteral("5"), QSerialPort::Data5);
    ui->CB_data_bits->addItem(QStringLiteral("6"), QSerialPort::Data6);
    ui->CB_data_bits->addItem(QStringLiteral("7"), QSerialPort::Data7);
    ui->CB_data_bits->addItem(QStringLiteral("8"), QSerialPort::Data8);
    ui->CB_data_bits->setCurrentIndex(3);

    ui->CB_parity->addItem(tr("None"), QSerialPort::NoParity);
    ui->CB_parity->addItem(tr("Even"), QSerialPort::EvenParity);
    ui->CB_parity->addItem(tr("Odd"), QSerialPort::OddParity);
    ui->CB_parity->addItem(tr("Mark"), QSerialPort::MarkParity);
    ui->CB_parity->addItem(tr("Space"), QSerialPort::SpaceParity);
    ui->CB_parity->setCurrentIndex(0);

    ui->CB_stop_bits->addItem(QStringLiteral("1"), QSerialPort::OneStop);
#ifdef Q_OS_WIN
    ui->CB_stop_bits->addItem(tr("1.5"), QSerialPort::OneAndHalfStop);
#endif
    ui->CB_stop_bits->addItem(QStringLiteral("2"), QSerialPort::TwoStop);
    ui->CB_stop_bits->setCurrentIndex(0);
}


void MainWindow::portOpened(){

    // reset receiver module with new connection
    serPtcl.resetReceiver();

    //DEACTIVATE SOME WIDGETS
    ui->CB_port->setEnabled(false);
    ui->CB_data_bits->setEnabled(false);
    ui->CB_parity->setEnabled(false);
    ui->CB_baud->setEnabled(false);
    ui->CB_stop_bits->setEnabled(false);
    ui->PB_refresh_ports->setEnabled(false);
    ui->PB_connect->setText(QString("DISCONNECT"));
}


void MainWindow::portClosed(){

    //ACTIVATE SOME WIDGETS
    ui->CB_port->setEnabled(true);
    ui->CB_data_bits->setEnabled(true);
    ui->CB_parity->setEnabled(true);
    ui->CB_baud->setEnabled(true);
    ui->CB_stop_bits->setEnabled(true);
    ui->PB_refresh_ports->setEnabled(true);
    ui->PB_connect->setText(QString("CONNECT"));
}



void MainWindow::PB_connect_ButtonClick(){

    serialPortThread::serialPortSettingsStc settings;

    if(serialThread.isRunning()){
        serialThread.disconnectPort();
    }
    else{
        settings.name     = ui->CB_port->currentText();
        settings.baudRate = static_cast<QSerialPort::BaudRate>(ui->CB_baud->itemData(ui->CB_baud->currentIndex()).toInt());
        settings.dataBits = static_cast<QSerialPort::DataBits>(ui->CB_data_bits->itemData(ui->CB_data_bits->currentIndex()).toInt());
        settings.parity   = static_cast<QSerialPort::Parity>  (ui->CB_parity->itemData(ui->CB_parity->currentIndex()).toInt());
        settings.stopBits = static_cast<QSerialPort::StopBits>(ui->CB_stop_bits->itemData(ui->CB_stop_bits->currentIndex()).toInt());

        if(settings.name.isEmpty())
            addNotification(tr("GUI: Port name is empty!\n"), Qt::darkRed);
        else
            serialThread.connectPort(settings);
    }

}


void MainWindow::PB_check_gui_ButtonClick(){

}


void MainWindow::PB_refresh_ports_ButtonClick(){

    ui->CB_port->clear();

    foreach (const QSerialPortInfo &info, QSerialPortInfo::availablePorts())
        ui->CB_port->addItem(info.portName());
}


quint16 extractDataFromField(QLineEdit * field){

    bool ok;
    quint16 temp16;

    temp16 = field->text().toInt(&ok,10);
    if(false == ok){
        return 0;
    }
    else{
        return temp16;
    }

}


void MainWindow::serialise32bitParameter(quint32 * var, QVector<quint8> * params)
{
    if(0 == var) return;
    if(0 == params) return;

    //serialise data
    params->append(*var     & 0xFF);
    params->append(*var>>8  & 0xFF);
    params->append(*var>>16 & 0xFF);
    params->append(*var>>24 & 0xFF);
}


void MainWindow::serialise16bitParameter(quint16 * var, QVector<quint8> * params)
{
    if(0 == var) return;
    if(0 == params) return;

    //serialise data
    params->append(*var     & 0xFF);
    params->append(*var>>8  & 0xFF);
}


void MainWindow::PB_template_ButtonClick(void)
{
    serialProtocol::messageStc message;

    message.cmdID = serialCommands::COMM_TEMPLATE;
    sendMessageToSerialPort(message);
}


void MainWindow::PB_startSensSelfCheck_ButtonClick(void)
{
    serialProtocol::messageStc message;

    message.cmdID = serialCommands::COMM_START_SELF_CHECK;
    sendMessageToSerialPort(message);
}


void MainWindow::PB_cal4ma_ButtonClick(void)
{
    quint32 val;

    serialProtocol::messageStc message;
    message.cmdID = serialCommands::COMM_SET_4MA_CAL;

    val = ui->periodSpinBox->value();

    serialise32bitParameter(&val, &message.parameters);
    sendMessageToSerialPort(message);
}


void MainWindow::PB_cal20ma_ButtonClick(void)
{
    quint32 val;

    serialProtocol::messageStc message;
    message.cmdID = serialCommands::COMM_SET_20MA_CAL;

    val = ui->periodSpinBox->value();

    serialise32bitParameter(&val, &message.parameters);
    sendMessageToSerialPort(message);
}


void MainWindow::PB_release4_20Override_ButtonClick(void)
{
    serialProtocol::messageStc message;
    message.cmdID = serialCommands::COMM_RELEASE_4_20_OVERRIDE;
    sendMessageToSerialPort(message);
}


void MainWindow::override4_20ma(const double &val)
{
    serialProtocol::messageStc message;
    float flVal = (float)val;

    message.cmdID = serialCommands::COMM_OVERRIDE_4_20_OUTPUT_MA;
    serialise32bitParameter((quint32*)&flVal, &message.parameters);
    sendMessageToSerialPort(message);
}


void MainWindow::override4_20period(const int &val)
{
    serialProtocol::messageStc message;
    quint32 uintVal;

    if(val < 0)
        uintVal = 0;
    else
        uintVal = val;

    message.cmdID = serialCommands::COMM_OVERRIDE_4_20_OUTPUT_PERIOD;
    serialise32bitParameter(&uintVal, &message.parameters);
    sendMessageToSerialPort(message);
}


void MainWindow::PB_relay1on_ButtonClick()
{
    serialProtocol::messageStc message;

    message.cmdID = serialCommands::COMM_SWITCH_RELAY_ON;
    message.parameters.append(1);  //append relay number
    sendMessageToSerialPort(message);
}


void MainWindow::PB_relay2on_ButtonClick()
{
    serialProtocol::messageStc message;

    message.cmdID = serialCommands::COMM_SWITCH_RELAY_ON;
    message.parameters.append(2);  //append relay number
    sendMessageToSerialPort(message);
}


void MainWindow::PB_relay1off_ButtonClick()
{
    serialProtocol::messageStc message;

    message.cmdID = serialCommands::COMM_SWITCH_RELAY_OFF;
    message.parameters.append(1);  //append relay number
    sendMessageToSerialPort(message);
}


void MainWindow::PB_relay2off_ButtonClick()
{
    serialProtocol::messageStc message;

    message.cmdID = serialCommands::COMM_SWITCH_RELAY_OFF;
    message.parameters.append(2);  //append relay number
    sendMessageToSerialPort(message);
}


void MainWindow::PB_releaseRelayOverride_ButtonClick()
{
    serialProtocol::messageStc message;

    message.cmdID = serialCommands::COMM_RELEASE_RELAY_OVERRIDE;
    sendMessageToSerialPort(message);
}


void MainWindow::PB_getDeviceId_ButtonClick()
{
    serialProtocol::messageStc message;

    message.cmdID = serialCommands::COMM_GET_DEVICE_ID;
    sendMessageToSerialPort(message);
}


void MainWindow::PB_getSystemStats_ButtonClick()
{
    serialProtocol::messageStc message;

    message.cmdID = serialCommands::COMM_SS_GET_STATS;
    sendMessageToSerialPort(message);
}


void MainWindow::PB_resetSystemStats_ButtonClick()
{
    serialProtocol::messageStc message;

    message.cmdID = serialCommands::COMM_SS_RESET;
    sendMessageToSerialPort(message);
}


