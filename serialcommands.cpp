

#include "serialcommands.h"


serialCommands::serialCommands(){

    //initialise command map
    this->commandsMap[COMM_NOTHING]              = "NOTHING";

    this->commandsMap[COMM_TRANSMISSION_ERROR]   = "TRANSMISSION ERROR";
    this->commandsMap[COMM_UNKNOWN_COMMAND]      = "UNKNOWN COMMAND";

    //diode driver IDs
    this->commandsMap[COMM_TEMPLATE]             = "TEMPLATE";

    this->commandsMap[COMM_START_SELF_CHECK]     = "START SELF CHECK";
    this->commandsMap[COMM_SELF_CHECK_STARTED]   = "SELF CHECK STARTED";
    this->commandsMap[COMM_SELF_CHECK_NOT_STARTED] = "SELF CHECK NOT STARTED";
    this->commandsMap[COMM_SELF_CHECK_PASS]      = "SELF CHECK PASS";
    this->commandsMap[COMM_SELF_CHECK_FAIL]      = "SELF CHECK FAIL";
    this->commandsMap[COMM_SELF_CHECK_TIMEOUT]   = "SELF CHECK TIMEOUT";
    this->commandsMap[COMM_SELF_CHECK_PENDING]   = "SELF CHECK PENDING";

    this->commandsMap[COMM_MODBUS_REG_30]        = "MODBUS REG 30";
    this->commandsMap[COMM_MODBUS_REG_80]        = "MODBUS REG 80";
    this->commandsMap[COMM_MODBUS_REG_82]        = "MODBUS REG 82";
    this->commandsMap[COMM_MODBUS_REG_84]        = "MODBUS REG 84";

    this->commandsMap[COMM_SET_4MA_CAL]                = "SET 4ma CALIBRATION";
    this->commandsMap[COMM_SET_20MA_CAL]               = "SET 20ma CALIBRATION";
    this->commandsMap[COMM_4_20_CAL_SET]               = "4-20 CALIBRATION SET";

    this->commandsMap[COMM_4_20_OVERRIDE_SET]          = "4-20 OVERRIDE SET";
    this->commandsMap[COMM_OVERRIDE_4_20_OUTPUT_MA]    = "OVERRIDE 4-20 OUTPUT [ma]";
    this->commandsMap[COMM_OVERRIDE_4_20_OUTPUT_PERIOD] = "OVERRIDE 4-20 OUTPUT [PERIOD]";
    this->commandsMap[COMM_RELEASE_4_20_OVERRIDE]      = "RELEASE 4-20 OVERRIDE";
    this->commandsMap[COMM_4_20_OVERRIDE_RELEASED]     = "4-20 OVERRIDE RELEASED";

    //relays test
    this->commandsMap[COMM_SWITCH_RELAY_ON]            = "SWITCH THE RELAY ON";
    this->commandsMap[COMM_SWITCH_RELAY_OFF]           = "SWITCH THE RELAY OFF";
    this->commandsMap[COMM_RELAY_SWITCHED]             = "RELAY SWITCHED";
    this->commandsMap[COMM_RELEASE_RELAY_OVERRIDE]     = "RELEASE RELAY OVERRIDE";
    this->commandsMap[COMM_RELAY_OVERRIDE_RELEASED]    = "RELAY OVERRIDE RELEASED";

    //system message IDs
    this->commandsMap[COMM_FUNCTION_ERROR]             = "FUNCTION ERROR";

    //system stats
    this->commandsMap[COMM_SS_RESET]                   = "RESET SYSTEM STATS";
    this->commandsMap[COMM_SS_RESET_DONE]              = "SYSTEM STATS RESET DONE";
    this->commandsMap[COMM_SS_GET_STATS]               = "GET SYSTEM STATS";
    this->commandsMap[COMM_SS_STATS]                   = "SYSTEM STATS";

    //device ID
    this->commandsMap[COMM_GET_DEVICE_ID]              = "GET DEVICE ID";
    this->commandsMap[COMM_DEVICE_ID]                  = "DEVICE ID";

}


serialCommands::~serialCommands(){
}


void serialCommands::processQueue(const QVector<serialProtocol::messageStc> &messages){

    for(int i=0;i<messages.size();i++){
        processMessage(messages[i]);
    }
}


void serialCommands::processMessage(const serialProtocol::messageStc &message){

    volatile templateParams_t * templateVal;
    volatile deviceIdParams_t * deviceIdPar;
    volatile systemStatsParams_t * ssPar;
    volatile quint32 * ptrUi32;
    volatile qint32  * ptrI32;
    volatile quint16 * ptrUi16;
    volatile qint16  * ptrI16;
    QString note;
    QString temp1, temp2;
    quint32 deviceId;
    systemStatsParams_t systemStats;

    //suppress compiler's warning
    (void)(templateVal);

    switch(message.cmdID){

    // FUNCTION TRIGGERS
    case COMM_TEMPLATE:
        //templateVal = (templateParams_t*)&message.parameters.at(0);
        note = "MCU: " % this->commandsMap[(commandsIds)message.cmdID] % "\n";
        emit notification(note, Qt::darkBlue);
        break;

    case COMM_DEVICE_ID:
        note = "MCU: " % this->commandsMap[(commandsIds)message.cmdID] % "\n";
        emit notification(note, Qt::darkBlue);

        deviceIdPar = (deviceIdParams_t*)&message.parameters.at(0);
        deviceId = deviceIdPar->deviceId;
        emit sendDeviceId(deviceId);
        break;

    case COMM_SS_STATS:
        note = "MCU: " % this->commandsMap[(commandsIds)message.cmdID] % "\n";
        emit notification(note, Qt::darkBlue);

        ssPar = (systemStatsParams_t*)&message.parameters.at(0);
        //for some reason I have to copy them one by one...
        systemStats.eepromHealth = ssPar->eepromHealth;
        systemStats.flashHealth  = ssPar->flashHealth;
        systemStats.proc    = ssPar->proc;
        systemStats.rxFail  = ssPar->rxFail;
        systemStats.sent    = ssPar->sent;
        emit sendSystemStats(systemStats);
        break;

    case COMM_MODBUS_REG_30:
        ptrUi16 = (quint16*)&message.parameters.at(0);
        ptrI16  = (qint16 *)&message.parameters.at(0);
        temp1.setNum(*ptrUi16, 16);
        temp2.setNum(*ptrI16, 10);
        note = "MCU: " % this->commandsMap[(commandsIds)message.cmdID] % ": " % temp2 % " 0x" % temp1 % "\n";
        emit notification(note, Qt::darkBlue);
        break;

    case COMM_MODBUS_REG_80:
    case COMM_MODBUS_REG_82:
    case COMM_MODBUS_REG_84:
        ptrUi32 = (quint32*)&message.parameters.at(0);
        ptrI32  = (qint32 *)&message.parameters.at(0);
        temp1.setNum(*ptrUi32, 16);
        temp2.setNum(*ptrI32, 10);
        note = "MCU: " % this->commandsMap[(commandsIds)message.cmdID] % ": " % temp2 % " 0x" % temp1 % "\n";
        emit notification(note, Qt::darkBlue);
        break;


    // BLUE COLOURED NOTIFICATIONS
    case COMM_START_SELF_CHECK:
    case COMM_SELF_CHECK_STARTED:
    case COMM_SELF_CHECK_NOT_STARTED:
    case COMM_SELF_CHECK_PASS:
    case COMM_SELF_CHECK_FAIL:
    case COMM_SELF_CHECK_TIMEOUT:
    case COMM_SELF_CHECK_PENDING:

    case COMM_4_20_CAL_SET:
    case COMM_4_20_OVERRIDE_SET:
    case COMM_4_20_OVERRIDE_RELEASED:

    case COMM_RELAY_SWITCHED:
    case COMM_RELAY_OVERRIDE_RELEASED:

    case COMM_FUNCTION_ERROR:

    case COMM_SS_RESET_DONE:
        note = "MCU: " % this->commandsMap[(commandsIds)message.cmdID] % "\n";
        emit notification(note, Qt::darkBlue);
        break;


    // RED COLOURED NOTIFICATIONS
    case COMM_NOTHING:
    case COMM_TRANSMISSION_ERROR:
    case COMM_UNKNOWN_COMMAND:
        note = "MCU: " % this->commandsMap[(commandsIds)message.cmdID] % "\n";
        emit notification(note, Qt::darkRed);
        break;

    // OMMITTED NOTIFICATIONS


    // UNRECOGNISED MESSAGES
    default:
        note = "GUI: Unknown command received! ID: " % tr("%1").arg(message.cmdID) % "\n";
        emit notification(note, Qt::darkRed);
        break;
    }
}



