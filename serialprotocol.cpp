
#include "serialprotocol.h"


serialProtocol::serialProtocol(){

    resetReceiver();
}

serialProtocol::~serialProtocol(){

}


void serialProtocol::resetReceiver(){
    receiver.state = FRAME_START;
    receiver.length = 0;
    receiver.paramCtr = 0;
    receiver.CRC = 0;
}


QVector<serialProtocol::messageStc> serialProtocol::addNewlyReceivedData(const QByteArray &data){

    QVector<messageStc> messages;
    quint16 temp;

    for(int i=0; i<data.size(); i++){

        temp = (quint8)data.data()[i];

        if(true == rxStateMachine(temp, &tempMessage)){
            messages.append(tempMessage);
        }
    }

    return messages;
}


bool serialProtocol::rxStateMachine(quint8 word, messageStc * message){
    bool status = false;

    switch(receiver.state){

    case FRAME_START:
        if(FRAME_START_SYMBOL == word){
            message->cmdID = 0;
            receiver.CRC = 0;
            message->parameters.clear();

            receiver.CRC ^= word;
            receiver.state = LENGTH;
        }
        break;


    case LENGTH:
        receiver.CRC ^= word;
        receiver.length = word;
        if(!(PARAM_LENGTH_LIMIT < receiver.length-1)) receiver.state = CMD_ID;
        else resetReceiver();
        break;


    case CMD_ID:
        receiver.CRC ^= word;
        message->cmdID = word;
        if(0 != (receiver.length-1)) receiver.state = PARAMS;
        else receiver.state = CRC;
        break;


    case PARAMS:
        receiver.CRC ^= word;
        message->parameters.append(word);
        receiver.paramCtr++;
        if(receiver.paramCtr == (receiver.length-1)) receiver.state = CRC;
        break;


    case CRC:
        if(receiver.CRC == word){
            status = true;
        }
        resetReceiver();
        break;

    case IDLE:
    default:
        break;
    }

    return status;
}


QByteArray spreadWord(quint16 word){
    QByteArray array;
    array.append((quint8)0xFF & word);
    array.append((quint8)0xFF & (word>>8));
    return array;
}


QByteArray serialProtocol::prepareTXmessage(const messageStc &message){
    QByteArray array;
    quint8 CRC = 0;

    array.append(FRAME_START_SYMBOL);
    CRC ^= FRAME_START_SYMBOL;

    array.append(message.parameters.size()+1);
    CRC ^= message.parameters.size()+1;

    array.append(message.cmdID);
    CRC ^= message.cmdID;

    for(int i=0; i<message.parameters.size(); i++){
        array.append(message.parameters.at(i));
        CRC ^= message.parameters.at(i);
    }

    array.append(CRC);

    return array;
}

