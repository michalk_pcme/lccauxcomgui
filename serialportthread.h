#ifndef SERIALPORTTHREAD_H
#define SERIALPORTTHREAD_H


#include <QThread>
#include <QMutex>
#include <QWaitCondition>
#include <QtSerialPort/QSerialPort>
#include <QColor>
#include <QTime>

#include "serialProtocol.h"

class serialPortThread : public QThread
{
    Q_OBJECT

public:

    typedef struct{
        QString name;
        qint32 baudRate;
        QSerialPort::DataBits dataBits;
        QSerialPort::Parity parity;
        QSerialPort::StopBits stopBits;
        QSerialPort::FlowControl flowControl;
    }serialPortSettingsStc;

    serialPortThread(QObject *parent = 0);
    ~serialPortThread();

    void disconnectPort();
    void connectPort(const serialPortSettingsStc &s);
    void sendData(const QByteArray &data);
    void run();

signals:
    void receivedData(const QByteArray &data);
    void notification(const QString &s, const QColor c);
    void portOpened();
    void portClosed();

private:
    serialPortSettingsStc settings;

    QVector<QByteArray> txData;
    //bool newTxDataFlag;

    QMutex mutex;
    bool quit;
};



#endif // SERIALPORTTHREAD_H

