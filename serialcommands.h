#ifndef SERIALCOMMANDS_H
#define SERIALCOMMANDS_H

#include <QObject>
#include <QColor>
#include <QMap>
#include <QVector>
#include <QFile>
#include <QTextStream>

#include "serialProtocol.h"


class serialCommands : public QObject{

    Q_OBJECT

public:

    typedef enum{
        //protocol specific IDs are from 0 to 9 are reserved
        //DO NOT USE ANY OF THOSE COMMAND IDS in code
        COMM_NOTHING 					= 0,

        COMM_TRANSMISSION_ERROR  		= 1,
        COMM_UNKNOWN_COMMAND 	 		= 2,


        //USER'S COMMAND ID SPACE
        //those IDs can be used in a code
        COMM_TEMPLATE 					= 10,

        COMM_START_SELF_CHECK           = 11,
        COMM_SELF_CHECK_STARTED         = 12,
        COMM_SELF_CHECK_NOT_STARTED     = 13,
        COMM_SELF_CHECK_PASS            = 14,
        COMM_SELF_CHECK_FAIL            = 15,
        COMM_SELF_CHECK_TIMEOUT         = 16,
        COMM_SELF_CHECK_PENDING         = 17,

        COMM_MODBUS_REG_30              = 20,
        COMM_MODBUS_REG_80              = 21,
        COMM_MODBUS_REG_82              = 22,
        COMM_MODBUS_REG_84              = 23,

        //4-20mA calibration
        COMM_SET_4MA_CAL                = 30,
        COMM_SET_20MA_CAL               = 31,
        COMM_4_20_CAL_SET               = 32,

        COMM_4_20_OVERRIDE_SET          = 33,
        COMM_OVERRIDE_4_20_OUTPUT_MA    = 34,
        COMM_OVERRIDE_4_20_OUTPUT_PERIOD = 35,
        COMM_RELEASE_4_20_OVERRIDE      = 36,
        COMM_4_20_OVERRIDE_RELEASED     = 37,

        //relays test
        COMM_SWITCH_RELAY_ON            = 40,
        COMM_SWITCH_RELAY_OFF           = 41,
        COMM_RELAY_SWITCHED             = 42,
        COMM_RELEASE_RELAY_OVERRIDE     = 43,
        COMM_RELAY_OVERRIDE_RELEASED    = 44,

        //system message IDs
        COMM_FUNCTION_ERROR             = 50,

        //system stats
        COMM_SS_RESET                   = 60,
        COMM_SS_RESET_DONE              = 61,
        COMM_SS_GET_STATS               = 62,
        COMM_SS_STATS                   = 63,

        COMM_GET_DEVICE_ID              = 70,
        COMM_DEVICE_ID                  = 71

    }commandsIds;


    #pragma pack(push,1)
    typedef struct{
        quint32  valA;
        quint8   valB;
        float    valC;
    }templateParams_t;
    #pragma pack(pop)


    #pragma pack(push,1)
    typedef struct{
        quint32  deviceId;
    }deviceIdParams_t;
    #pragma pack(pop)


    #pragma pack(push,1)
    typedef struct
    {
        quint8 eepromHealth;
        quint8 flashHealth;
        quint32 sent;
        quint32 proc;
        quint32 rxFail;
    }systemStatsParams_t;
    #pragma pack(pop)


    QMap<commandsIds, QString> commandsMap;


    serialCommands();
    ~serialCommands();

    void processQueue(const QVector<serialProtocol::messageStc> &messages);

signals:
    void notification(const QString &s, const QColor color);
    void sendMessage(const serialProtocol::messageStc &message);
    void sendDeviceId(const quint32 &deviceId);
    void sendSystemStats(const serialCommands::systemStatsParams_t &systemStats);

    void stateQuery(void);

private:
    void processMessage(const serialProtocol::messageStc &message);
};



#endif // SERIALCOMMANDS_H

