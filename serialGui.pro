#-------------------------------------------------
#
# Project created by QtCreator 2015-07-22T09:17:12
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets printsupport

QT += serialport

DEFINES *= QT_USE_QSTRINGBUILDER

TARGET = serialGui
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp \
    serialportthread.cpp \
    serialprotocol.cpp \
    serialcommands.cpp

HEADERS  += mainwindow.h \
    serialportthread.h \
    serialprotocol.h \
    serialcommands.h

FORMS    += mainwindow.ui
