
#include "serialportthread.h"
#include "serialprotocol.h"



QT_USE_NAMESPACE

serialPortThread::serialPortThread(QObject *parent)
    : QThread(parent), quit(false)
{

}


serialPortThread::~serialPortThread()
{
    mutex.lock();
    quit = true;
    mutex.unlock();
    wait();
}


void serialPortThread::disconnectPort(){
    mutex.lock();
    quit = true;
    mutex.unlock();
}


void serialPortThread::connectPort(const serialPortSettingsStc &s)
{
    if (!isRunning()){
        this->quit = false;
        this->settings = s;
        start();
    }
}


void serialPortThread::sendData(const QByteArray &data){

    if (!isRunning())
        return;

    mutex.lock();
    this->txData.append(data);
    mutex.unlock();
}


void serialPortThread::run()
{
    bool locNewTxDataFlag = false;
    QSerialPort serial;
    QString locPortName;
    QByteArray locTxData;


    // JUST IN CASE PROTECT SHARED SETTINGS RESOURCE
    mutex.lock();
    locPortName = settings.name;
    serial.setPortName(settings.name);
    serial.setBaudRate(settings.baudRate);
    serial.setDataBits(settings.dataBits);
    serial.setParity(settings.parity);
    serial.setStopBits(settings.stopBits);
    mutex.unlock();


    // TRY OPENING SERIAL PORT
    if (!serial.open(QIODevice::ReadWrite)) {
        emit notification(tr("GUI: Can't open %1, error code %2\n").arg(locPortName).arg(serial.error()), Qt::darkRed);
        return;
    }
    else{
        emit portOpened();
        emit notification(tr("GUI: Port %1 has been opened successfully.\n").arg(locPortName), Qt::darkGreen);
    }


    // RECEPTION/TRANSMISSION LOOP
    while (!quit) {

        // COPY NEW REQUEST IF THERRE IS ANY
        mutex.lock();
        if(false == this->txData.empty()){
            locTxData = this->txData[0];
            locNewTxDataFlag = true;
            this->txData.pop_front();
        }
        mutex.unlock();


        // HANDLE SERIAL PORT ERRORS
        switch(serial.error()){
        case QSerialPort::DeviceNotFoundError:
        case QSerialPort::ResourceError:
        case QSerialPort::OpenError:
        case QSerialPort::PermissionError:
        case QSerialPort::WriteError:
        case QSerialPort::ReadError:
        case QSerialPort::UnsupportedOperationError:
        case QSerialPort::UnknownError:
        case QSerialPort::NotOpenError:
            emit notification(tr("GUI: Port %1, error code: %2\n").arg(locPortName).arg(serial.error()), Qt::darkRed);
            emit notification(tr("GUI: Port %1, has been closed!\n").arg(locPortName),Qt::darkRed);
            serial.close();
            emit portClosed();
            return;
            break;

        case QSerialPort::ParityError:
        case QSerialPort::FramingError:
        case QSerialPort::BreakConditionError:
        case QSerialPort::TimeoutError:
            emit notification(tr("GUI: Port %1, error code: %2\n").arg(locPortName).arg(serial.error()), Qt::darkRed);
            serial.clearError();
            break;

        case QSerialPort::NoError:
        default:
            break;
        }


        // read response
        if (serial.waitForReadyRead(10)) {
            QByteArray data = serial.readAll();
            emit this->receivedData(data);
        }
        else{
            if(QSerialPort::TimeoutError == serial.error()) serial.clearError();
        }


        // write request
        if(true == locNewTxDataFlag){
            serial.waitForBytesWritten(5000);
            serial.write(locTxData);
            locNewTxDataFlag = false;
        }


    }

    // close serial port after finishing loop
    serial.close();
    emit portClosed();
    emit notification(tr("GUI: Port %1 has been closed.\n").arg(locPortName), Qt::darkGreen);
}
