#ifndef SERIALPROTOCOL_H
#define SERIALPROTOCOL_H

#include <QVector>

#define FRAME_START_SYMBOL      0x42     // 'B'
#define PARAM_LENGTH_LIMIT      (128-3)  // bytes



class serialProtocol{

public:
    typedef enum{
        FRAME_START,
        LENGTH,
        CMD_ID,
        PARAMS,
        CRC,
        IDLE
    }frameStates;


    typedef struct{
        quint8 cmdID;
        QVector<quint8> parameters;
    }messageStc;


    serialProtocol();
    ~serialProtocol();

    QVector<messageStc> addNewlyReceivedData(const QByteArray &data);
    QByteArray prepareTXmessage(const messageStc &message);
    void resetReceiver(void);

private:
    struct{
        frameStates state;
        quint8 length;     //length of the command ID and parameters altogether
        quint8 paramCtr;
        quint8 CRC;
    }receiver;

    messageStc tempMessage;

    bool rxStateMachine(quint8, messageStc*);
};





#endif // SERIALPROTOCOL_H

