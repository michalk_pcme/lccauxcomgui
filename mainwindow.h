#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include "serialportthread.h"
#include "serialprotocol.h"
#include "serialcommands.h"
#include <QColor>



//QT_USE_NAMESPACE
QT_BEGIN_NAMESPACE

class QLabel;
class QLineEdit;
class QSpinBox;
class QPushButton;
class QComboBox;
class QCheckBox;

namespace Ui {
class MainWindow;
}

QT_END_NAMESPACE

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:

    typedef enum{
        WORD32BIT,
        FLOAT32BIT,
        HALF16BIT
    }dataTypes;

    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

private slots:

    void processMessages(const QByteArray &data);

    void portOpened();
    void portClosed();

    void displayNotification(const QString &message, const QColor color);

    void sendMessageToSerialPort(const serialProtocol::messageStc &message);
    void displayDeviceId(const quint32 &deviceId);

    void displaySystemStats(const serialCommands::systemStatsParams_t &stats);

    void PB_connect_ButtonClick();
    void PB_check_gui_ButtonClick();
    void PB_refresh_ports_ButtonClick();

    void PB_clear_notifications_ButtonClick();

    void serialise32bitParameter(quint32 * var, QVector<quint8> * params);
    void serialise16bitParameter(quint16 * var, QVector<quint8> * params);

    void PB_template_ButtonClick();

    void PB_startSensSelfCheck_ButtonClick();

    void PB_cal4ma_ButtonClick();
    void PB_cal20ma_ButtonClick();
    void PB_release4_20Override_ButtonClick();

    void override4_20ma(const double &val);
    void override4_20period(const int &val);

    void PB_relay1on_ButtonClick();
    void PB_relay2on_ButtonClick();
    void PB_relay1off_ButtonClick();
    void PB_relay2off_ButtonClick();
    void PB_releaseRelayOverride_ButtonClick();

    void PB_getDeviceId_ButtonClick();

    void PB_getSystemStats_ButtonClick();
    void PB_resetSystemStats_ButtonClick();


private:
    void addNotification(const QString &message, const QColor &color);
    void connectSignals(void);
    void fillComboBoxes(void);


private:
    Ui::MainWindow *ui;
    serialProtocol serPtcl;
    serialCommands serCmd;
    serialPortThread serialThread;

};

#endif // MAINWINDOW_H
